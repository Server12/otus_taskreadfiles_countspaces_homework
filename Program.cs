﻿using System.Diagnostics;
using System.Text;

namespace Otus.Homework;

class Program
{
    static async Task Main(string[] args)
    {
        if (args.Length == 0)
        {
            Console.WriteLine("Enter Folder Path with txt files:");
            var path = Console.ReadLine();
            if (Directory.Exists(path))
            {
                Console.WriteLine($"Read files from:{path}");
                await ReadAllTextFilesAndCountSpaces(path);
            }
        }

        Console.ReadLine();

        var projectDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources");
        Console.WriteLine($"Read local files from:{projectDir}");
        await ReadAllTextFilesAndCountSpaces(projectDir);
    }

    private static async Task ReadAllTextFilesAndCountSpaces(string folderPath)
    {
        try
        {
            if (Directory.Exists(folderPath))
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                var allFiles = Directory.GetFiles(folderPath, "*.txt");
                var list = new List<Task>();
                foreach (var file in allFiles)
                {
                    list.Add(ReadFileAsync(file));
                }

                await Task.WhenAll(list);

                stopWatch.Stop();
                Console.WriteLine($"Read {allFiles.Length} txt files and count spaces elapsed:{stopWatch.Elapsed}");
            }
            else
            {
                Console.WriteLine("Folder path is empty, or not exist");
            }
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
        }
    }


    private static async Task ReadFileAsync(string filePath)
    {
        try
        {
            var data = await File.ReadAllTextAsync(filePath, Encoding.UTF8);
            var spacesCount = CountSpaceCharacters(data);

            Console.WriteLine($"Total space characters:{spacesCount} in file:{filePath}");
        }
        catch (Exception e)
        {
            Console.WriteLine($"Read File Error:{e.Message}");
        }
    }

    private static int CountSpaceCharacters(string content)
    {
        int counter = 0;
        if (string.IsNullOrEmpty(content)) return counter;
        foreach (var ch in content)
        {
            if (ch == ' ')
            {
                counter++;
            }
        }

        return counter;
    }
}